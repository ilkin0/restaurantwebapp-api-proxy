# Restaurant demo web app API proxy

NGINX proxy app for demo restaurant web app

## Usage

### Environment variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`) 

