server {
    listen ${LISTEN_PORT};

    location ~\.(png|ico|gif|jpg|jpeg|css|js)$ {
        	{# root /src/main/resources/static; #}
            alias /vol/static;
	}

    location / {
        proxy_pass          ${APP_HOST}:${APP_PORT};
        {# proxy_set_header    X-Real-IP $remote_addr; #}
        {# proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for; #}
        {# proxy_set_header    Host $http_host; #}

        client_max_body_size 10M;
    }
}